
//1- Capturar dados da tela
var pizzas = [] //criação do array


function cadastrar() {
    var pizza: [string, number, number, number];
    var nome: string = (document.getElementById("idNome") as HTMLInputElement).value //fazer para os 3 campos
    var tamanho: number = Number((document.getElementById("idTamanho") as HTMLInputElement).value)
    var preco: number = Number((document.getElementById("idPreco") as HTMLInputElement).value)

    pizza.push(nome) 
    pizza.push(tamanho)
    pizza.push(preco)


    //2 --- calcular preço do cm² está numa função abaixo --calculo
    var precoCm2: number = calcularPrecoCm2(preco, tamanho)
    pizza.push(precoCm2)

    pizzas.push(pizza) // quatro dados: nome, tamanho, preço e custo cm²
}



//2- Calcular preço/cm² --calculo
function calcularPrecoCm2(preco: number, tamanho: number) {
    var area: number = 3.14 * (tamanho / 2) ** 2
    var precoCm2: number = preco / area
    return precoCm2
}


function exibirRel() {

    //3- Ordenar os dados (do mais barato para o mais caro), com base no Preco/cm²

    pizzas.sort(function (a: number, b: number) : number {
        if (a[3] > b[3]) {
            return 1
        }
        if (b[3] > a[3]) {
            return -1
        }
        return 0
    })



    //4 - Calcular a diferença percentual entre pizzas
    //criar um array percorrendo o pizzas linha a linha calculando diferça percentual do valor por cm² da linha atual com a proxima linha 
    //push na linha

    pizzas[0].push("Melhor CB")
    for (let i = 0; i < pizzas.length - 1; i++) {
        var precoCm2A: number = pizzas[i][3]
        var precoCm2B: number = pizzas[i + 1][3]

        var diffPercentual: number = ((precoCm2B / precoCm2A) - 1) * 100

        pizzas[i + 1].push(diffPercentual)
    }

    //5 - Exibir o relatório (criar a tabela na tela)

    var tbodyRel: HTMLElement = document.getElementById("tboIdLinha")
    for (let i = 0; i < pizzas.length; i++) {
        var linha: HTMLTableRowElement = montaTr(pizzas[i])
        tbodyRel.appendChild(linha)
    }

}

//Cria UMA linha (um tr)
function montaTr(pizza) {
    let pizzaTr: HTMLTableRowElement = document.createElement("tr")
    pizzaTr.classList.add("tdNomeData")

    console.log(pizza[3]);
    console.log(pizza[3].toFixed(2));

    pizzaTr.appendChild(montaTd(pizza[0], "tdNome"))
    pizzaTr.appendChild(montaTd(pizza[1], "tdTamanho"))
    pizzaTr.appendChild(montaTd(pizza[2], "tdPreco"))
    pizzaTr.appendChild(montaTd(pizza[3].toFixed(5), "tdValor"))

    if (isNaN(pizza[4])) {
        pizzaTr.appendChild(montaTd(pizza[4], "tdDiferenca"))
    } else {
        pizzaTr.appendChild(montaTd(pizza[4].toFixed(2), "tdDiferenca"))
    }

    return pizzaTr
}

//Cria UMA coluna (um td)
function montaTd(dado: any, classe: string) {
    let pizzaTd: HTMLTableCellElement = document.createElement("td")
    pizzaTd.classList.add(classe)

    pizzaTd.textContent = dado

    return pizzaTd
}



